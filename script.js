"use strict";

const accordComp = document.querySelectorAll(".accordion__component--js");

accordComp.forEach((elem) => {
  elem.addEventListener("click", toggleActive);
});

function toggleActive() {
  accordComp.forEach((elem) => elem.classList.remove("active"));

  this.classList.add("active");
}
